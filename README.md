# tests

### Production Build

 ```bash
 ng build --prod
 ```

### Run UI in docker container

```bash
$ docker-compose -f docker-compose.prod.yml up --build
docker build --build-arg APP_ENV=prod -t tag-name .
```
